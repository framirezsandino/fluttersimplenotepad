import 'package:intl/intl.dart';

class DateUtil {
  static String getDateTimeFromDateString(String datestring) {
    var date = DateTime.parse(datestring);
    var formatter = new DateFormat('yyyy-MM-dd - HH:mm a');
    return formatter.format(date);
  }

  static String getDateStringFromDateTime(DateTime date) {
    if (date == null) {
      return null;
    } else {
      var formatter = new DateFormat('yyyy-MM-dd - HH:mm a');
      return formatter.format(date);
    }
  }
}
