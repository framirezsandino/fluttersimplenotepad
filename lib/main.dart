import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttersimplenotepad2/event-details.dart';
import 'package:fluttersimplenotepad2/persistence/dao/event.dart';
import 'package:fluttersimplenotepad2/persistence/dao/note.dart';
import 'package:fluttersimplenotepad2/persistence/persistence-strategy.dart';
import 'package:fluttersimplenotepad2/persistence/sqlite-provider.dart';
import 'package:fluttersimplenotepad2/util/date-util.dart';

PersistenceStrategy ps;
NoteDAO currentNote;

void main() async {
  currentNote = new NoteDAO.fromObject(new Note.empty());
  ps = new SQLiteStrategy();
  await ps.open();
  runApp(MyApp());
}

FlutterLocalNotificationsPlugin flutterLocalNotificationPlugin;

class MyApp extends StatelessWidget {
  MyApp() {}
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Eventos',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Eventos'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime selectedDay;
  String title;
  List<Note> _notes = [];
  String filterMessage = '';

  @override
  initState() {
    super.initState();
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
            title: new Text(title),
            content: new Text(body),
            actions: [
              CupertinoDialogAction(
                isDefaultAction: true,
                child: new Text('Ok'),
                onPressed: () async {
                  Navigator.of(context, rootNavigator: true).pop();
                  currentNote.getNote(int.parse(payload)).then((event) async {
                    await Navigator.push(
                      context,
                      new EventDetails(event),
                    );
                  });
                },
              )
            ],
          ),
    );
  }

  Future onSelectNotification(String payload) async {
    print('payload: $payload');
    if (payload != null) {
      currentNote.getNote(int.parse(payload)).then((event) {
        ps.update(new NoteDAO.fromObject(event)).then((onValue) async {
          await Navigator.push(
            context,
            new EventDetails(event),
          );
        });
      });
    }
  }

  _MyHomePageState() {
    this._reloadList();
  }

  void _setSelectedDay(DateTime sd) {
    setState(() {
      selectedDay = sd;
    });
  }

  void _reloadList() {
    currentNote.getAll().then((value) {
      setState(() {
        _notes = value;
      });
    });
  }

  void _deleteList() {
    currentNote.deleteAll().then((onValue) {
      _reloadList();
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title), actions: <Widget>[]),
      body: Flex(
        direction: Axis.vertical,
        children: <Widget>[
          Text(filterMessage),
          Expanded(
            child: ListView.builder(
              itemCount: _notes.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) {
                final note = _notes[index];
                return ListTile(
                  leading: CircleAvatar(
                    child: Text(note.title[0].toUpperCase()),
                    backgroundColor: Colors.blue,
                  ),
                  title: Text(note.title),
                  subtitle: Text(
                      'Fecha: ${DateUtil.getDateStringFromDateTime(note.created)}'),
                  onTap: () =>
                      Navigator.of(context).push(new EventDetails(note)),
                );
              },
            ),
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Opciones (solo debug)'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Recargar lista'),
              onTap: () {
                _reloadList();
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Borrar todos los eventos'),
              onTap: () {
                _deleteList();
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
