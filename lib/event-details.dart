import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttersimplenotepad2/main.dart';
import 'package:fluttersimplenotepad2/persistence/dao/event.dart';
import 'package:fluttersimplenotepad2/persistence/dao/note.dart';
import 'package:fluttersimplenotepad2/util/date-util.dart';

class EventDetails extends MaterialPageRoute<Null> {
  final Note _note;

  static _addEventNotification(Note note) async {
    var notificationId = 'eventos';
    var notificationSecondaryId = 'eventos_academicos';
    var notificationDescription = 'Eventos académicos enviados desde la app';
    var scheduledNotificationDateTime;

    var date = note.created;
    // If event date is located in less than 2 days the, send notification after 5 seconds,
    // otherwise schedule it two days before the event's date
    if (DateTime.now()
            .subtract(new Duration(days: 2))
            .difference(date)
            .inSeconds <=
        0) {
      scheduledNotificationDateTime =
          DateTime.now().add(new Duration(seconds: 5));
    } else {
      scheduledNotificationDateTime = date;
    }
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        notificationId, notificationSecondaryId, notificationDescription);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    NotificationDetails platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    flutterLocalNotificationPlugin
        .schedule(note.id, '${note.title} - ${note.content}', '',
            scheduledNotificationDateTime, platformChannelSpecifics,
            payload: note.id.toString())
        .then((onValue) {
      ps.update(new NoteDAO.fromObject(note));
    });
  }

  EventDetails(this._note)
      : super(builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              title: Text('Detalles'),
            ),
            body: ListView(
              children: <Widget>[
                ListTile(
                  title: Text('${_note.title}'),
                ),
                Table(columnWidths: const <int, TableColumnWidth>{
                  0: FixedColumnWidth(64.0),
                }, children: <TableRow>[
                  TableRow(children: <Widget>[
                    Icon(Icons.title),
                    Text('Título:'),
                    Text('${_note.title}')
                  ]),
                  TableRow(children: <Widget>[
                    Icon(Icons.place),
                    Text('Lugar:'),
                    Text('${_note.content}')
                  ]),
                  TableRow(children: <Widget>[
                    Icon(Icons.calendar_today),
                    Text('Fecha:'),
                    Text('${DateUtil.getDateStringFromDateTime(_note.created)}')
                  ]),
                ]),
              ],
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                _addEventNotification(_note);
              },
              child: Icon(Icons.notifications),
              backgroundColor: Colors.blue,
            ),
          );
        });
}
