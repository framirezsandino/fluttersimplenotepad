import 'package:fluttersimplenotepad2/constants.dart';
import 'package:fluttersimplenotepad2/persistence/dao/dao.dart';
import 'package:fluttersimplenotepad2/persistence/dao/db.dart';
import 'package:fluttersimplenotepad2/persistence/persistence-strategy.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'dao/sqlite.dart/tables.dart';

class SQLiteStrategy implements PersistenceStrategy {
  Database db;

  static final SQLiteStrategy _sqliteProvider = new SQLiteStrategy._internal();

  factory SQLiteStrategy() {
    return _sqliteProvider;
  }

  SQLiteStrategy._internal() {
    this.createTableIfNotExists(notesTable);
  }

  Future<void> createTableIfNotExists(Table table) {
    List<String> sqlParams = [];
    sqlParams.add('id integer primary key autoincrement');
    sqlParams.addAll(table.params.values
        .map((f) => '${f.name} ${f.type} ${f.notNull ? "not null" : ""}')
        .toList());
    var sql = sqlParams.join(",");
    return db.execute('create table if not exists ${table.name} ($sql)');
  }

  Future<Database> open() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, SQLiteDatabasePath);
    if (db == null || !db.isOpen) {
      db = await openDatabase(path, version: 1);
    }
    return db;
  }

  Future<int> insert(DAO dao) async {
    return db.insert(dao.tableName, dao.params);
  }

  Future<List<Map>> select(DAO dao) async {
    if (db == null) {
      await open();
    }
    return db.query(dao.tableName, columns: dao.params.keys, where: 'id = ?');
  }

  Future<int> delete(DAO dao) async {
    if (db == null) {
      await open();
    }
    return await db.delete(dao.tableName, where: 'id = ?', whereArgs: [dao.id]);
  }

  Future<bool> deleteAll(String tableName) async {
    if (db == null) {
      await open();
    }
    await db.rawQuery('DELETE FROM "$tableName"');
    return true;
  }

  Future<int> update(DAO dao) async {
    return await db.update(dao.tableName, dao.params,
        where: 'id = ?', whereArgs: [dao.id]);
  }

  Future close() async => db.close();

  Future<List<Map>> selectFromId(int id, String tableName) async {
    if (db == null) {
      await open();
    }
    List<Map> maps = await db
        .query(tableName, columns: null, where: '$id = ?', whereArgs: [id]);
    return maps;
  }

  Future<List<Map>> selectAll(String tableName) async {
    return await db.rawQuery('SELECT * FROM $tableName');
  }
}
