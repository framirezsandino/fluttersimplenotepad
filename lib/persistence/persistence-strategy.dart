import 'package:fluttersimplenotepad2/persistence/dao/dao.dart';
import 'package:fluttersimplenotepad2/persistence/dao/db.dart';

abstract class PersistenceStrategy {
  Future<void> createTableIfNotExists(Table table);
  Future<dynamic> open();
  Future<int> insert(DAO dao);
  Future<List<Map>> select(DAO dao);
  Future<int> delete(DAO dao);
  Future<bool> deleteAll(String tableName);
  Future<int> update(DAO dao);
  Future close();

  Future<List<Map>> selectAll(String tableName);
  Future<List<Map>> selectFromId(int id, String tableName);
}
