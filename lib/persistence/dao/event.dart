import 'package:fluttersimplenotepad2/persistence/dao/dao.dart';
import 'package:fluttersimplenotepad2/persistence/persistence-strategy.dart';
import 'package:fluttersimplenotepad2/persistence/sqlite-provider.dart';

final String tableName = 'events';

final String columnId = 'title';
final String columnTitle = 'title';
final String columnPlace = 'place';
final String columnEventDate = 'eventDate';
final String columnEventType = 'eventType';
final String columnOrganizers = 'organizers';
final String columnNotificationActive = 'notify';

class Event {
  int id;
  String title;
  String place;
  String date;
  String type;
  String organizers;
  bool notificationsActive;

  Event(this.id, this.title, this.place, this.date, this.type, this.organizers,
      this.notificationsActive);

  Event.empty();

  Event.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    title = map[columnTitle];
    place = map[columnPlace];
    type = map[columnEventType];
    organizers = map[columnOrganizers];
    date = map[columnEventDate];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnTitle: title,
      columnPlace: place,
      columnEventDate: date,
      columnEventType: type,
      columnOrganizers: organizers,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

class EventDAO extends DAO {
  PersistenceStrategy ps = new SQLiteStrategy();

  Event get event {
    return new Event.fromMap(params);
  }

  EventDAO(int id, String title, String place, DateTime date, String type,
      String organizers, bool notificationsActive) {
    var map = <String, dynamic>{
      columnTitle: title,
      columnPlace: place,
      columnEventDate: date,
      columnEventType: type,
      columnOrganizers: organizers,
    };
    EventDAO._fromParams(id, tableName, map);
  }

  EventDAO.fromObject(Event event)
      : super.fromParams(event.id, tableName, event.toMap());

  EventDAO._fromParams(int id, tableName, Map<String, dynamic> params)
      : super.fromParams(id, tableName, params);

  Future<Event> getEvent(int id) async {
    var map = await ps.selectFromId(id, tableName);
    if (map.length > 0) {
      return Event.fromMap(map[0]);
    }
    return null;
  }

  Future<bool> deleteAll() async {
    return ps.deleteAll(tableName);
  }

  @override
  Future<List> getAll() async {
    var map = await ps.selectAll(tableName);
    List<Event> events = [];
    map.toList().forEach((f) {
      events.add(new Event.fromMap(f));
    });
    return events;
  }
}
