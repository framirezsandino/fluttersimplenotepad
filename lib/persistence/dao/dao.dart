import 'package:fluttersimplenotepad2/persistence/dao/note.dart';

abstract class DAO {
  int id;
  String tableName;
  Map<String, dynamic> params;

  DAO();
  DAO.fromParams(int id, String tableName, Map<String, dynamic> params);
  Future<List<dynamic>> getAll();
  Future<bool> deleteAll();
}
