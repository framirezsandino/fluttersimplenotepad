class Table {
  String name;
  Map<String, Attribute> params;
  Table(this.name, this.params);
}

class Attribute {
  String name;
  String type = 'text';
  bool notNull = false;
  Attribute(this.name, this.type, this.notNull);
}
