import 'package:fluttersimplenotepad2/persistence/dao/db.dart';

Table notesTable = new Table('notes', {
  'title': new Attribute('title', 'text', true),
  'content': new Attribute('content', 'text', true),
  'created': new Attribute('created', 'numeric', true),
  'updated': new Attribute('updated', 'numeric', true)
});
