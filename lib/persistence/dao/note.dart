import 'package:fluttersimplenotepad2/persistence/dao/dao.dart';
import 'package:fluttersimplenotepad2/persistence/persistence-strategy.dart';
import 'package:fluttersimplenotepad2/persistence/sqlite-provider.dart';

final String tableName = 'notes';

final String columnId = 'id';
final String columnTitle = 'title';
final String columnContent = 'content';
final String columnCreated = 'created';
final String columnUpdated = 'updated';

class Note {
  int id;
  String content;
  String title;
  DateTime created; // Stored as miliseconds on persistence
  DateTime updated; // Stored as miliseconds on persistence

  Note(this.id, this.content, this.title) {
    created = DateTime.now();
    updated = DateTime.now();
  }

  Note.empty();

  Note.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    title = map[columnTitle];
    content = map[columnContent];
    // int or DateTime
    created = (map[columnCreated] is int)
        ? DateTime.fromMicrosecondsSinceEpoch(map[columnCreated])
        : map[columnCreated];
    updated = (map[columnCreated] is int)
        ? DateTime.fromMicrosecondsSinceEpoch(map[columnUpdated])
        : map[columnCreated];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnTitle: title,
      columnContent: content,
      columnCreated: created,
      columnUpdated: updated
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

class NoteDAO extends DAO {
  PersistenceStrategy ps = new SQLiteStrategy();

  Note get note {
    return new Note.fromMap(super.params);
  }

  NoteDAO.fromObject(Note note)
      : super.fromParams(note.id, tableName, note.toMap());

  Future<Note> getNote(int id) async {
    var map = await ps.selectFromId(id, tableName);
    if (map.length > 0) {
      return Note.fromMap(map[0]);
    }
    return null;
  }

  Future<List<Note>> getAll() async {
    var map = await ps.selectAll(tableName);
    List<Note> events = [];
    map.toList().forEach((f) {
      events.add(new Note.fromMap(f));
    });
    return events;
  }

  Future<bool> deleteAll() async {
    return ps.deleteAll(tableName);
  }
}
